import javax.swing.*;
import java.awt.*;

public class CenterPanelDemo extends JFrame {

    public CenterPanelDemo() {
// Create a panel with a red background
        JPanel redPanel = new JPanel();
        redPanel.setBackground(Color.RED);
        redPanel.setPreferredSize(new Dimension(200, 100)); // Set preferred size

// Create another panel with a blue background
        JPanel bluePanel = new JPanel(new GridBagLayout()); // Use BorderLayout
        bluePanel.setBackground(Color.BLUE);

// Add the red panel to the center of the blue panel
        bluePanel.add(redPanel);

// Add the blue panel to the content pane of the frame
        this.add(bluePanel);

// Set frame properties
        this.setTitle("Center Panel Demo");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.pack();
        this.setLocationRelativeTo(null); // Center on screen
        this.setVisible(true);
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(CenterPanelDemo::new);
    }
}
