import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class Test {
    public static void main(String[] args) {
        JFrame frame = new JFrame("test");
        JPanel panel = new JPanel(null);
        JPanel panel1 = new JPanel();
        JPanel panel2 = new JPanel();
        panel.add(panel1);
        panel.add(panel2);
        frame.add(panel);
        panel.setBackground(Color.LIGHT_GRAY);
        panel1.setBackground(Color.PINK);
        panel2.setBackground(Color.ORANGE);
        panel1.setBounds(0, 0, 50, 50);
        panel2.setBounds(50, 50, 50, 50);
        panel1.addMouseListener(dragListener);
        panel2.addMouseListener(dragListener);
        panel1.addMouseMotionListener(dragListener);
        panel2.addMouseMotionListener(dragListener);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setSize(300, 200);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }

    static MouseAdapter dragListener = new MouseAdapter() {
        private Point lastPoint = null;

        @Override
        public void mousePressed(MouseEvent e) {
            lastPoint = e.getLocationOnScreen();
        }

        @Override
        public void mouseDragged(MouseEvent e) {
            Component source = (Component) e.getSource();
            Point point = e.getLocationOnScreen();
            int offsetX = point.x - lastPoint.x;
            int offsetY = point.y - lastPoint.y;
            Rectangle bounds = source.getBounds();
            bounds.x += offsetX;
            bounds.y += offsetY;
            source.setBounds(bounds);
            lastPoint = point;
        }

    };
}
