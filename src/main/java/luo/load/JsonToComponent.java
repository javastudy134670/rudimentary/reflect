package luo.load;

import com.alibaba.fastjson2.JSONArray;
import com.alibaba.fastjson2.JSONObject;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class JsonToComponent {
    public static Object load(String json) throws ClassNotFoundException, NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException, NoSuchFieldException {
        JSONObject jsonObject = JSONObject.parseObject(json);
        String className = jsonObject.getString("class");
        Object colorObject = jsonObject.get("color");
        JSONArray location = jsonObject.getJSONArray("location");
        JSONArray size = jsonObject.getJSONArray("size");
        JSONArray child = jsonObject.getJSONArray("child");
        String text = jsonObject.getString("text");
        Class<?> clazz = Class.forName(className);
        Class<?> colorType = Class.forName("java.awt.Color");
        Method setBounds = clazz.getMethod("setBounds", int.class, int.class, int.class, int.class);
        Method add = clazz.getMethod("add", Class.forName("java.awt.Component"));
        Object instance = clazz.getDeclaredConstructor().newInstance();
        setBounds.invoke(instance, location.getIntValue(0), location.getIntValue(1), size.getIntValue(0), size.getIntValue(1));
        if (colorObject != null) {
            Object color = null;
            if (colorObject instanceof String colorName) {
                color = colorType.getField(colorName.toUpperCase()).get(null);
            } else if (colorObject instanceof JSONArray array) {
                color = colorType.getDeclaredConstructor(int.class, int.class, int.class).newInstance(array.getIntValue(0), array.getIntValue(1), array.getIntValue(2));
            }
            Method setBackGround = clazz.getMethod("setBackground", colorType);
            setBackGround.invoke(instance, color);
        }
        if (child != null) {
            Method setLayout = clazz.getMethod("setLayout", Class.forName("java.awt.LayoutManager"));
            setLayout.invoke(instance, (Object) null);
            for (var each : child) {
                add.invoke(instance, load(each.toString()));
            }
        }
        if (text != null) {
            Method setText = clazz.getMethod("setText", String.class);
            setText.invoke(instance, text);
        }
        return instance;
    }
}