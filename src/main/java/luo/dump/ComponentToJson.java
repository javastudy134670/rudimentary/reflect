package luo.dump;

import com.alibaba.fastjson2.JSONArray;
import com.alibaba.fastjson2.JSONObject;

import java.awt.*;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class ComponentToJson {
    public static JSONObject dump(Component component) throws InvocationTargetException, IllegalAccessException {
        JSONObject json = new JSONObject();
        json.put("class", component.getClass().getName());
        Color color = component.getBackground();
        json.put("color", new JSONArray(color.getRed(), color.getGreen(), color.getBlue()));
        json.put("location", new JSONArray(component.getLocation().x, component.getLocation().y));
        json.put("size", new JSONArray(component.getWidth(), component.getHeight()));
        if (component instanceof Container container) {
            JSONArray array = new JSONArray();
            for (Component containerComponent : container.getComponents()) {
                array.add(dump(containerComponent));
            }
            if (!array.isEmpty()) {
                json.put("child", array);
            }
        }
        Method getText = null;
        try {
            getText = component.getClass().getMethod("getText");
        } catch (NoSuchMethodException ignore) {
        }
        if (getText != null) {
            String text = getText.invoke(component).toString();
            json.put("text", text);
        }
        return json;
    }
}
