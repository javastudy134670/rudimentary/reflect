package luo;

import luo.load.JsonToComponent;

import java.awt.*;
import java.io.FileInputStream;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

public class ConfigurableGUI {
    public static void main(String[] args) throws IOException, NoSuchFieldException, ClassNotFoundException, InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        FileInputStream inputStream = new FileInputStream("config.json");
        byte[] bytes = inputStream.readAllBytes();
        inputStream.close();
        String json = new String(bytes);
        Component component = (Component) JsonToComponent.load(json);
        component.setVisible(true);
    }
}
