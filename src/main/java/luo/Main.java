package luo;

import luo.dump.ComponentToJson;
import luo.load.JsonToComponent;

import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

public class Main {
    public static void main(String[] args) throws IOException, InvocationTargetException, IllegalAccessException {
        JFrame frame1 = new JFrame("origin");
        JPanel panel1 = new JPanel(null);
        JPanel panel2 = new JPanel();
        JButton button1 = new JButton("你好");
        JButton button2 = new JButton("再见");
        JLabel label = new JLabel("你若安好，便是晴天");
        panel1.setBackground(Color.LIGHT_GRAY);
        panel2.setBackground(Color.ORANGE);
        button1.setBackground(Color.CYAN);
        button2.setBackground(Color.PINK);
        frame1.setContentPane(panel1);
        panel1.add(button1);
        panel1.add(button2);
        panel1.add(panel2);
        panel2.add(label);
        button1.setBounds(0, 0, 100, 50);
        button2.setBounds(60, 60, 100, 50);
        panel2.setBounds(50, 120, 200, 30);
        frame1.setBounds(200, 200, 320, 200);
        frame1.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame1.setVisible(true);
        String json = ComponentToJson.dump(panel1).toJSONString();
        File file = new File("component.json");
        if (!file.exists() && !file.createNewFile()) {
            throw new RuntimeException("creat error");
        }
        FileWriter writer = new FileWriter(file);
        writer.write(json);
        writer.close();
        Component component;
        try {
            component = (Component) JsonToComponent.load(json);
        } catch (ClassNotFoundException | NoSuchFieldException | InstantiationException | NoSuchMethodException e) {
            throw new RuntimeException(e);
        }
        JFrame frame2 = new JFrame("result");
        frame2.add(component);
        frame2.setBounds(200, 200, 320, 200);
        frame2.setLocationRelativeTo(null);
        frame2.setVisible(true);
    }
}
