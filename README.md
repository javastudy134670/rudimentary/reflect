# ConfigurableGUI

author: 罗浩

## 功能介绍

dump包的ComponentToJson 用于将Component转化为Json对象，其中如果存在子组件，将会把所有子组件加入child属性。

load包的JsonToComponent 用于将Json字符串转化为Component。

## 存在的问题

dump存在着一些问题，我将子组件加入child时用到的是getComponents方法，这样的话如果组件的类是自带子组件的话，当load时那些自带的组件会被重复添加。
例如：JFrame类本身带了一个RootPanel的子组件，其它组件都是放置在RootPanel之下的，这样的话dump得到的json文件中会包含这个RootPanel，而load的时候也将会在这个frame中添加RootPanel，就会重复添加。
因此dump仅支持非容器组件，以及不自带子组件的容器组件。

## 实现细节

想要精准地将组件加入到容器地特定位置中，需要取消这个容器的默认layout，即采用绝对布局。setLayout(null)。然后让子组件使用setBounds

为了准确模仿反射的应用情形，load包中的 JsonToComponent.java 仅导入了反射所需的包，以及解析json的包。

json中颜色属性既可以是rgb的整数数组，也可以是颜色的名称（颜色名称将采用反射的方式获得Color类中的静态属性），也可以不写，采用默认颜色。dump时采用的是rgb方式。

json中text属性是针对于文字域、标签、按钮等带文本组件。

## 功能截图

Main 类运行结果。将origin窗口中的 panel load 成 json 文件，然后dump成组件加入result窗口，可见完美还原。

![](screenshots/image1.png)

上图中涉及到的json文件产物

![](screenshots/image2.png)

手写一个json配置文件，意欲将其实现出来，注意此时我可以写入JFrame等负责组件了。

![](screenshots/image3.png)

延续上图，在ConfigurableGUI类的运行结果。所有组件和细节都真实还原了。

![](screenshots/image4.png)